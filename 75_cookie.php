<?php
    // before any HTML, unless output_bufferring On.
	    $name = "mytest";
	    $value = 45;
		$expire = time() + (60*60*24*7); // 7 days in seconds
	    // setcookie($name, $value, $expire);
        
		// php задаёт новое значение для браузера,
        // оно будет установлено на странице в $_COOKIE и заголовках 
		// при следующем запросе.		
?>
<!DOCTYPE html>
<html>
<head>
    <title>Cookies</title>
</head>
<body>
    <pre>
	<?php
	   // print_r($_COOKIE);
	   /*
	   $test = $_COOKIE["mytest"];
	   echo $test;
	   */
	   
	   /*
	   if (isset($_COOKIE["mytest"])) {
		   $test = $_COOKIE["mytest"];
	   } else {
		   $test = "";
	   }
	   echo $test;
	   */
	   
	   $test = isset($_COOKIE["mytest"]) ? $_COOKIE["mytest"] : "" ;
	   // php читает то, что уже хранится в $_COOKIE.
	   echo $test;
	?>
	</pre>

</body>
</html> 