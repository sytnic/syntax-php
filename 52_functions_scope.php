<!DOCTYPE html>
<html>
<head>
    <title>Functions: Scope</title>
</head>
<body>
        <?php		
			$bar = "outside";   // global scope			
			function foo() {		
				$bar = "inside";  // local scope
			}		
			echo "1: " . $bar . "<br />"; // outside
			foo();
			echo "2: " . $bar . "<br />"; // outside		
		?>
    <br> <br>
	    <?php		
			$bar = "outside";   // global scope			
			function foo2($bar) {
				if (isset($bar)) {
					echo "foo: " . $bar . "<br />";
				}
				$bar = "inside";  // local scope
			}		
			echo "1: " . $bar . "<br />"; // outside
			foo2($bar);                   // outside
			echo "2: " . $bar . "<br />"; // outside		
		?>
    <br> <br>
		<?php		
			$bar = "outside";   // global scope			
			function foo3() {
				global $bar;
				if (isset($bar)) {
					echo "foo: " . $bar . "<br />";
				}
				$bar = "inside";  // not local scope
			}		
			echo "1: " . $bar . "<br />"; // outside
			foo3();                       // outside
			echo "2: " . $bar . "<br />"; // inside, функция отработала и перезаписала значение глобальной переменной $bar		
		?>

</body>
</html> 