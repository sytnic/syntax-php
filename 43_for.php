<!DOCTYPE html>
<html>
<head>
    <title>For</title>
</head>
<body>
    <?php // while loop example
		  $count = 0;
		  while ($count <= 10) {
		    echo $count . ", ";
		    $count++;
		  }
	?>
	<br /><br />
	<?php		
			for($count = 0; $count <= 10; $count++) {
		    echo $count . ", ";
			}		
	?>		
	<br /><br />
	<?php
		$fix = 1;
		while ($fix < 20) {
			if ($fix % 2 == 0) {
				echo "{$fix} is even<br>";
			} else {
				echo "{$fix} is odd<br>";				
			}			
			$fix++;
		}
    ?>
	<br /><br />
    <?php		
		
		    for ($fix = 1; $fix < 20; $fix++) {
				if ($fix % 2 == 0) {
				echo "{$fix} is even<br>";
			    } else {
				echo "{$fix} is odd<br>";				
			    }		
			}

    ?>
</body>
</html> 