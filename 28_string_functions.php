<!DOCTYPE html>
<html>
<head>
    <title>Strings Functions</title>
</head>
<body>
    <?php
	
    $first = "The quick brown fox";
	$second = " jumped over the lazy dog.";
	
	$third = $first;
	$third .= $second;
	echo $third;
	// конкатенация строки
	
	?>
	<br /><br />
	Lowercase: <?php echo strtolower($third); ?><br />
	Uppercase: <?php echo strtoupper($third); ?><br />
	Uppercase first: <?php echo ucfirst($third); ?><br />
	Uppercase words: <?php echo ucwords($third); ?><br />
	<br /><br />
	Length: <?php echo strlen($third); 
	// 45	?>
	<br />
	Trim: <?php echo "A" . trim(" B C D ") . "E"; 
	// AB C DE	?>
	<br />
	Find: <?php echo strstr($third, "brown"); 
	// brown fox jumped...	?>
	<br />
	Replace by string: <?php echo str_replace("quick", "super-fast", $third); ?>
	<br /><br />
	Repeat: <?php echo str_repeat($third, 2); ?><br />
	Make substring: <?php echo substr($third, 5, 10); 
	// uick brown	?>
	<br />
	Find position: <?php echo strpos($third, "brown"); 
	// 10	?>
	<br />
	Find character: <?php echo strchr($third, "z"); 
	// zy dog.	?>
	<br />
	

</body>
</html> 