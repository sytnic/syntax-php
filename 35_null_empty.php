<!DOCTYPE html>
<html>
<head>
    <title>NULL</title>
</head>
<body>
		<?php
			$var1 = null;
			$var2 = "";
		?>
		var1 null? <?php echo is_null($var1); // 1 ?><br />
		var2 null? <?php echo is_null($var2); // false, т.к. имеет значение "" ?><br />
		var3 null? <?php echo is_null($var3); // 1, хотя и не задана ?><br />
		<br />
		<!-- isset == "задана" или "имеет ли значение" -->
		var1 is set? <?php echo isset($var1); // false  ?><br />
		var2 is set? <?php echo isset($var2); // true 1 ?><br />
		var3 is set? <?php echo isset($var3); // false  ?><br />
		<br />
		
		<?php // empty: "", null, 0, 0.0, "0", false, array() ?>
		
		<?php $var3 = "0"; ?>
		var1 empty? <?php echo empty($var1); // 1 ?><br />
		var2 empty? <?php echo empty($var2); // 1 ?><br />
		var3 empty? <?php echo empty($var3); // 1 ?><br />


</body>
</html> 