<!DOCTYPE html>
<html>
<head>
    <title>Type Juggling and Type Casting</title>
</head>
<body>
        Type Juggling<br />
		<?php $count = "2 cats"; ?>
		
		Type: <?php echo gettype($count); // string ?><br />
		
		<?php $count += 3; ?>
		Type: <?php echo gettype($count); // integer, возможно notice ?><br />
		
		<?php $cats = "I have " . $count /* 5 */. " cats."; ?>
		Cats: <?php echo gettype($cats); // string ?><br />
		<br />
		
		Type Casting<br />
		<?php var_dump($count); // int 5 
		    // $count участвовало в строке, но строкой не стало
			// можно было не приводить тип далее
		?><br />		
		<?php settype($count, "integer"); ?>
		count: <?php echo gettype($count); ?><br />
		<br />
		<?php $count2 = (string) $count; ?>
		count: <?php echo gettype($count);   // integer ?><br />
		count2: <?php echo gettype($count2); // string  ?><br />
		<br />
		<!-- settype() меняет тип переменной, (string)$var не меняет тип $var -->
		<!-- к примеру: -->
		<?php $test1 = 3; ?>
		<?php $test2 = 3; ?>
		<?php settype($test1, "string"); // settype перезаписывает тип ?>
		<?php (string) $test2; // просто инструкция, не перезаписывает тип ?>        
		test1: <?php echo gettype($test1);  // поэтому string  ?><br />
		test2: <?php echo gettype($test2);  // поэтому integer ?><br />
		
</body>
</html> 