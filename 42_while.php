<!DOCTYPE html>
<html>
<head>
    <title>While</title>
</head>
<body>
    <?php
			$count = 0;
			while ($count <= 10) {
				if ($count == 5) {
					echo "FIVE, ";
				} else {
					echo $count . ", ";
				}
				$count++;  // increment by 1
			}
			echo "<br />";
			echo "Count: {$count}";	
    ?><br>
	<?php
	    $fix = 1;
		while ($fix < 20) {
			if ($fix % 2 == 0) {
				echo "{$fix} is even<br>";
			} else {
				echo "{$fix} is odd<br>";				
			}			
			$fix++;
		}		
	?>

</body>
</html> 