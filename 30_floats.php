<!DOCTYPE html>
<html>
<head>
    <title>Floats</title>
</head>
<body>
        <?php echo $float = 3.14; ?><br />
		<?php echo $float + 7; // 10.14 ?><br />
        <?php echo 4/3; ?><br />
		
		<br />
		Round: 		<?php echo round($float, 1); // 3,1 ?><br />
		Ceiling: 	<?php echo ceil($float); // потолок, 4  ?><br />
		Floor: 		<?php echo floor($float); // пол, 3    ?><br />
		<br />
		
		<?php $integer = 3; ?>
		
		<!-- Будут выведены булевы 1 (true) или ничего (false). -->
		<?php echo "Is {$integer} integer? " . is_int($integer); ?><br />
		<?php echo "Is {$float} integer? " . is_int($float); ?><br />
		<br/>
		<?php echo "Is {$integer} integer? " . is_integer($integer); ?><br />
		<?php echo "Is {$float} integer? " . is_integer($float); ?><br />
		<br />
		
		<?php echo "Is {$integer} float? " . is_float($integer); ?><br />
		<?php echo "Is {$float} float? " . is_float($float); ?><br />
		<br />
		<?php echo "Is {$integer} numeric? " . is_numeric($integer); ?><br />
		<?php echo "Is {$float} numeric? " . is_numeric($float); ?><br />
		<br />

</body>
</html> 