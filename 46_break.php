<!DOCTYPE html>
<html>
<head>
    <title>Break</title>
</head>
<body>
        <?php
			for ($count=0; $count <= 10; $count++) {
				if ($count == 5) {
					break; // завершает все итерации и цикл
				}
				echo $count . ", ";
			}
		?>
		
		<br />
		<?php // loop inside a loop with break
			for ($i=0; $i<=5; $i++) {
				if ($i % 2 == 0) { continue(1); }
				for ($k=0; $k<=5; $k++) {
					if ($k == 3) { break(1); // прекращает текущий цикл
					}
			  	echo $i . "-" . $k . "<br />";
				}
			}
		?>
		<br />
		<?php // loop inside a loop with break
			for ($i=0; $i<=5; $i++) {
				if ($i % 2 == 0) { continue(1); }
				for ($k=0; $k<=5; $k++) {
					if ($k == 3) { break(2); // прекращает цикл одним уровнем выше
					}
			  	echo $i . "-" . $k . "<br />";
				}
			}
		?>



</body>
</html> 