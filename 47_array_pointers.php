<!DOCTYPE html>
<html>
<head>
    <title>Array Pointers</title>
</head>
<body>
        <?php
		
			$ages = array(4,8,15,16,23,42);
		
			// current: current pointer value
			echo "1: " . current($ages) . "<br />"; // 4
		
			// next: move the pointer forward
			// similar to using 'continue' inside a loop
			next($ages);
			echo "2: " . current($ages) . "<br />"; // 8
			
			next($ages); // шаг вперёд
			next($ages);
			echo "3: " . current($ages) . "<br />"; // 16
			
			// prev: move the pointer backward
			prev($ages); // шаг назад
			echo "4: " . current($ages) . "<br />"; // 15
			
			// reset: move the pointer to the first element
			reset($ages);  // сброс, в начало
			echo "5: " . current($ages) . "<br />"; // 4

			// end: move the pointer to the last element
			end($ages);  // в конец
			echo "6: " . current($ages) . "<br />"; // 42
	
			// move the pointer past the last element
			next($ages); // за пределы конца
			echo "7: " . current($ages) . "<br />"; // null
			
		?>
		<br />
		<?php
			reset($ages);
			
			// while loop that moves the array pointer
			// similar to foreach
			while($age = current($ages)) {
				echo $age . ", ";
				next($ages);
			}
		?>

</body>
</html> 