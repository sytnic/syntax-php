<?php
    // before any HTML, unless output_bufferring On.
	    $name = "mytest";
	    $value = "hello";
		$expire = time() + (60*60*24*7); // 7 days in seconds
	    //setcookie($name, $value, $expire);
        
		// php задаёт новое значение для браузера,
        // оно будет установлено на странице в $_COOKIE и заголовках 
		// при следующем запросе.

		// 1) сброс значений, кука стирается в браузере
        //setcookie($name);

		// 2) кука стирается в браузере
		//setcookie($name, null, $expire);
		
		// 3) кука стирается в браузере
		//setcookie($name, $value, time()-3600);
		
		// 4) combine 2+3, кука стирается в браузере
		//setcookie($name, null, time()-3600);
?>
<!DOCTYPE html>
<html>
<head>
    <title>Cookies</title>
</head>
<body>
    <pre>
	<?php	  
	   
	   $test = isset($_COOKIE["mytest"]) ? $_COOKIE["mytest"] : "" ;
	   // php читает то, что уже хранится в $_COOKIE.
	   echo $test;
	?>
	</pre>

</body>
</html> 