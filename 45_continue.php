<!DOCTYPE html>
<html>
<head>
    <title>Continue</title>
</head>
<body>
        <?php
			for ($count=0; $count <= 10; $count++) {
				if ($count == 5) { 
				    continue; // завершает итерацию цикла, 
					// возвращает к началу цикла;
					// continue как бы всегда есть в конце любого цикла
				}
				echo $count . ", ";
			}
		?>
	<br>
		<?php
			for ($count=0; $count <= 10; $count++) {
				if ($count % 2 == 0) { continue; }
				echo $count . ", ";
			}
		?>
    <br>  
	    <?php // what's wrong with this?
			$count = 0;
			while ($count <= 10) {
				if ($count == 5) {
					$count++;
					continue;
				}
				echo $count . ", ";
				$count++;
			}
		?>
    <br> 
		<?php // loop inside a loop with continue

			for ($i=0; $i<=5; $i++) {
				if ($i % 2 == 0) { continue(1); }
				for ($k=0; $k<=5; $k++) {
					if ($k == 3) { continue(2); } // возвращает на 2-ой цикл вверх
			  	echo $i . "-" . $k . "<br />";
				}
			}
		?>

</body>
</html> 