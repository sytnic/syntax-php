<!DOCTYPE html>
<html>
<head>
    <title>Validation Errors</title>
</head>
<body>
    <?php
	
	    $errors = array();
	
	    // * presence
		$value = trim("");
		if (!isset($value) || empty($value)) {
			$errors['value'] = "Value can't be blank.";
		}
						
		// * string length
		// minimum length
		$value = "abcd";
		$min = 3;
		if (strlen($value) < $min) {
			echo "Validation failed, too minimum length. <br />";
		}
		
		// max length
		$max = 6;
		if (strlen($value) > $max) {
			echo "Validation failed, too max length. <br />";
		}
		
		// * type
		$value = "1";
		if (!is_string($value)) {
			echo "Validation failed, not a string.<br />";
		}
		
		// * inclusion in a set
		$value = "1";
		$set = array("1", "2", "3", "4");
		if (!in_array($value, $set)) {
			echo "Validation failed, it's not in array.<br />";
		}
		
		// * uniqueness
		// uses a database to check uniqueness
		
		// * format
		// use a regex on a string
		// preg_match($regex, $subject)		
		$value = "nobody@nowhere.com";
		// preg_match is most flexible
		if (!preg_match("/@/", $value)) {
			echo "Validation failed. preg_match @ <br />";
		}
		
		// strpos is faster than preg_match
		// use === to make exact match with false
		if (strpos($value, "@") === false) {
		  echo "Validation failed. strpos @ <br />";
		}	
		
		// print_r($errors);
		
		
			//if (!empty($errors)) {
				// redirect_to("first_page.php");
                //  or
			// 	include("form.php");
			// } else {
			// 	include("success.php");
			// }
			
		/*
				if (!empty($errors)) {
				  echo "<div class=\"error\">";
				  echo "Please fix the following errors:";
				  echo "<ul>";
				  foreach ($errors as $key => $error) {
				    echo "<li>{$error}</li>";
				  }
				  echo "</ul>";
				  echo "</div>";
				}		
		*/
		
		    function form_errors($errors=array()) { 
			    $output = "";
				if (!empty($errors)) {
				  $output .= "<div class=\"error\">";
				  $output .=  "Please fix the following errors:";
				  $output .=  "<ul>";
				  foreach ($errors as $key => $error) {
				    $output .=  "<li>{$error}</li>";
				  }
				  $output .=  "</ul>";
				  $output .=  "</div>";
				}		
		        return $output;
            }		
		
		echo form_errors($errors);		
		
	?>
</body>
</html> 