<?php 
// Sessions use cookies, which use in headers.
// So that, start before any HTML,
// unless output_buffering On.
session_start(); 
?>
<!DOCTYPE html>
<html>
<head>
    <title>Session</title>
</head>
<body>
    <?php
	    $_SESSION["first_name"] = "Dmitry";
		$name =  $_SESSION["first_name"];
		echo $name;
		
		// простой сброс пары значений, кука сессии сохраняется в браузере
		// $_SESSION["first_name"] = null;	
	?> 
</body>
</html> 