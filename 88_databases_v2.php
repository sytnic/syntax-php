<?php
  // 1. Create a database connection
  $dbhost = "localhost";
  $dbuser = "widget_user";
  $dbpass = "secretpassword";
  $dbname = "widget_db";
  $connection = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
  // Test if connection occurred.
  if(mysqli_connect_errno()) { // return error code or 0
    die("Database connection failed: " . 
         mysqli_connect_error() . 
         " (" . mysqli_connect_errno() . ")"
    );
  }
?>
<?php
  // 2. Perform database query
  
  // $query = "SELECT * FROM subjects"; // or so:
    $query  = "SELECT * ";
	$query .= "FROM subjects ";
	$query .= "WHERE visible = 1 ";
	$query .= "ORDER BY position ASC";
  $result = mysqli_query($connection, $query);
  // $result == resource
  // Test if there was a query error
  if (!$result) {
	  die("Database query failed.");
  }
?>
<!DOCTYPE html>
<html>
<head>
    <title>Databases</title>
</head>
<body>

<ul>  
<?php
  // 3. Use returned data (if any)
  /*  mysqli_fetch_row($result)
      mysqli_fetch_assoc($result)
	  mysqli_fetch_array($result)
	  mysqli_fetch_array($result, MYSQLI_ASSOC)
	  mysqli_fetch_array($result, MYSQLI_NUM)
	  mysqli_fetch_array($result, MYSQLI_BOTH)  
  */  

  while($subject = mysqli_fetch_assoc($result)) {
	  // output data from each row
	  ?>
	  <li><?php echo $subject["menu_name"]." (".$subject["id"].")"; ?></li>
	  <?php
  }	
?>
</ul>

<?php
  // 4. Release returned data
  // This is necessary for only resource (for SELECT)
  mysqli_free_result($result);
?>
</body>
</html> 
<?php
  // 5. Close database connection
  mysqli_close($connection);
?>