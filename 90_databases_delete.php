<?php
  // 1. Create a database connection
  $dbhost = "localhost";
  $dbuser = "widget_user";
  $dbpass = "secretpassword";
  $dbname = "widget_db";
  $connection = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
  // Test if connection occurred.
  if(mysqli_connect_errno()) { // return error code or 0
    die("Database connection failed: " . 
         mysqli_connect_error() . 
         " (" . mysqli_connect_errno() . ")"
    );
  }
?>
<?php
    // Often these are form values in $_POST
	$id = 4;

   // 2. Perform database query  
    $query  = "DELETE FROM subjects ";	
	$query .= "WHERE id = {$id} ";
	$query .= "LIMIT 1";
	
  $result = mysqli_query($connection, $query);
  // $result == true/false
  
  if ($result && mysqli_affected_rows($connection) == 1) {
	  // mysqli_affected_rows returns -1, if it was failure
	  // and returns 0, if no rows was changed
	  
	  //Success
	  // redirect_to("somepage.php");
	  echo "Success!";
  } else {
	  // Failure
	  // $message = "Subject delete failed";
	  die("Database query failed.".mysqli_error($connection));
  }
?>
<!DOCTYPE html>
<html>
<head>
    <title>Databases</title>
</head>
<body>


</body>
</html> 
<?php
  // 5. Close database connection
  mysqli_close($connection);
?>