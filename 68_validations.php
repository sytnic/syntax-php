<!DOCTYPE html>
<html>
<head>
    <title>Validations</title>
</head>
<body>
    <?php
	    // * presence
		$value = "x";
		if (!isset($value) || empty($value)) {
			echo "Validation failed, not presence.<br />";
		}
		
		// empty()
		$value = "00";
		if (empty($value)) {
			echo "Validation failed, empty.<br />"; 
			// empty: "", 0, "0", null, false, array()
			// not empty: " ", 1, "00" ...
		}
		
		// v.2 empty()
		$value = trim("  0  ");
		var_dump($value);
		
		$iss = !isset($value);
		var_dump($iss); // " 0 " - false
		$emp = empty($value);
		var_dump($emp); // " 0 " - true
		$num = !is_numeric($value);
		var_dump($num); // " 0 " - false; it's numeric, see https://www.php.net/manual/ru/function.is-numeric
		
		
		if (!isset($value) || (empty($value) && !is_numeric($value))) {
			// " 0 ", (false || ((true + false) == false)) == false, and not {echo..}
			echo "Validation failed, trim-v2 empty.<br />";
		}
		echo "<br><br>";
		// v.3 empty()
		$value = trim("  ");
		$iss = !isset($value);
		var_dump($iss); // " 0 "- false; "  " - false, because isset.
		if (!isset($value) || $value === "") {
			// " 0 ", (false || false) == false, and not {echo..}
			// "  ",  (false || true) == true and {echo..}
			echo "Validation failed, trim-v3 empty.<br />";
		}
		
		// * string length
		// minimum length
		$value = "abcd";
		$min = 3;
		if (strlen($value) < $min) {
			echo "Validation failed, too minimum length. <br />";
		}
		
		// max length
		$max = 6;
		if (strlen($value) > $max) {
			echo "Validation failed, too max length. <br />";
		}
		
		// * type
		$value = "1";
		if (!is_string($value)) {
			echo "Validation failed, not a string.<br />";
		}
		
		// * inclusion in a set
		$value = "1";
		$set = array("1", "2", "3", "4");
		if (!in_array($value, $set)) {
			echo "Validation failed, it's not in array.<br />";
		}
		
		// * uniqueness
		// uses a database to check uniqueness
		
		// * format
		// use a regex on a string
		// preg_match($regex, $subject)
		if (preg_match("/PHP/", "PHP is fun.")) {
			echo "A match was found.";
		} else {
		  echo "A match was not found.";
		}
		
		$value = "nobody@nowhere.com";
		// preg_match is most flexible
		if (!preg_match("/@/", $value)) {
			echo "Validation failed. preg_match @ <br />";
		}
		
		// strpos is faster than preg_match
		// use === to make exact match with false
		if (strpos($value, "@") === false) {
		  echo "Validation failed. strpos @ <br />";
		}	
	?>
</body>
</html> 