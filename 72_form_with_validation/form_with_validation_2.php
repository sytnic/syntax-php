<?php
    // Validations editing

    require_once("../67_form_single/included_functions.php");
	require_once("../71_validation_functions/validation_functions.php");
	
	$errors = array();
	$message = "";

    if (isset($_POST['submit'])) {
		// form was submitted
		$username = trim($_POST['username']);	
		$password = trim($_POST['password']);	
		
		// Validations
		$fields_required = array("username", "password");
		
        foreach ($fields_required as $field) {			
			$value = trim($_POST[$field]);				
			if (!has_presence($value)) {
			    $errors[$field] = ucfirst($field)." can't be blank.";
		    }		
		}
		
		// Using an assoc. array
		$fields_with_max_lengths = array("username" => 12, "password" => 8);
		
		foreach ($fields_with_max_lengths as $field => $max) {
			$value = trim($_POST[$field]);
			if (!has_max_length($value, $max)) {
			    $errors[$field] = ucfirst($field)." is too long.";	
			}	
		}
		
		
        // If has not errors	
		if (empty($errors)) {
			// try to login
			if ($username == "kevin" && $password == "secret") {
				// succesful login
				redirect_to("../basic.html");
			} else {
                // failed login	
				$message = "Username/password do not match.";			
		    }			
		}
		
	// was not $_POST	
	} else {
		$username = "";
		$message = "Please, log in.";
	}	
?>

<!DOCTYPE html>
<html>
<head>
    <title>Form</title>
</head>
<body>

        <?php echo $message; ?>
		<?php echo form_errors($errors); ?>
	
        <form action="form_with_validation_2.php" method="post">
		  Username: <input type="text" name="username" value="<?php echo htmlspecialchars($username); ?>" /><br />
		  Password: <input type="password" name="password" value="" /><br />
			<br />
		  <input type="submit" name="submit" value="Submit" />
		</form>

</body>
</html>