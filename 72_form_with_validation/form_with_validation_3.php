<?php
    // Validations "max_lengths" taken out in the functions

    require_once("../67_form_single/included_functions.php");
	require_once("../71_validation_functions/validation_functions.php");
	
	// 1
	$errors = array();
	$message = "";
    
	// 2
    if (isset($_POST['submit'])) {
		// form was submitted
		$username = trim($_POST['username']);	
		$password = trim($_POST['password']);	
	
    // 3	
		// Validations
		// find errors
		$fields_required = array("username", "password");
		
        foreach ($fields_required as $field) {			
			$value = trim($_POST[$field]);				
			if (!has_presence($value)) {
			    $errors[$field] = ucfirst($field)." can't be blank.";
		    }		
		}
		
		// Using an assoc. array
		$fields_with_max_lengths = array("username" => 12, "password" => 8);
		
		validate_max_length($fields_with_max_lengths);
		
	// 4	
        // If has not errors	
		if (empty($errors)) {
			// try to login
			if ($username == "kevin" && $password == "secret") {
				// succesful login
				redirect_to("../basic.html");
			} else {
                // failed login	
				$message = "Username/password do not match.";			
		    }			
		}
	
	// 2
	// was not $_POST	
	} else {
		$username = "";
		$message = "Please, log in.";
	}	
?>

<!DOCTYPE html>
<html>
<head>
    <title>Form</title>
</head>
<body>
        <?php // 5 ?>
        <?php echo $message; // from 1 or 2, or 4 step?>
		<?php echo form_errors($errors); // from 3 step?>
	
	    <?php // 6 ?>
        <form action="form_with_validation_3.php" method="post">
		  Username: <input type="text" name="username" value="<?php echo htmlspecialchars($username); ?>" /><br />
		  Password: <input type="password" name="password" value="" /><br />
			<br />
		  <input type="submit" name="submit" value="Submit" />
		</form>

</body>
</html>